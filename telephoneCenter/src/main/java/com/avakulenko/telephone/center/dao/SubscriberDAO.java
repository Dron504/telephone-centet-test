package com.avakulenko.telephone.center.dao;


import com.avakulenko.telephone.center.domain.Subscriber;

import java.util.List;

public interface SubscriberDAO {
    //create
    /*int*/void insert(Subscriber account);

    //get
    Subscriber getById(int id);

    // update
    void update(Subscriber subscriber);

    // delete
    void delete(int subId);

    // get all
    List<Subscriber> getAll();

    Integer checkUserCredentials(String login, String password);
}
