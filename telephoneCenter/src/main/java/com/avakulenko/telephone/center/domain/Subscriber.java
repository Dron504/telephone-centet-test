package com.avakulenko.telephone.center.domain;


import java.util.List;
import java.util.Set;

/**
 *
 */

public class Subscriber extends User {

    private boolean isBlocked;
    private String number;
    private double balance;
    private List<Serv> servs;
    private Set<Account> accounts;

    public Subscriber() {
    }

    public Subscriber(String firstName, String lastName, String login, String password) {
        super(firstName, lastName, login, password);
        isBlocked = false;
    }


    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public List<Serv> getServs() {
        return servs;
    }

    public void setServs(List<Serv> servs) {
        this.servs = servs;
    }

    public Set<Account> getAccountOlds() {
        return accounts;
    }

    public void setAccountOlds(Set<Account> accounts) {
        this.accounts = accounts;
    }

    public boolean isBlocked() {
        return isBlocked;
    }

    public boolean isBlocked(boolean blocked) {
        isBlocked = blocked;
        return isBlocked;
    }

    public double getBalance() {
        return balance;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }

}
