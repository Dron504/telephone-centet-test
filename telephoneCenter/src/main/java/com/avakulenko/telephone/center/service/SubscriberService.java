package com.avakulenko.telephone.center.service;

import com.avakulenko.telephone.center.domain.Serv;
import com.avakulenko.telephone.center.domain.Subscriber;

import java.util.List;

/**
 * Created by Andrey on 17.03.2016.
 */
public interface SubscriberService {

    void addSubscriber(Subscriber subscriber);

    Subscriber getById(int subId);

    void removeSubscriber(int subId);

    List<Subscriber> getAllSubscriber();

    void blockSubscriber(int subId);

    void unBlockSubscriber(int subId);

    List<Serv> getAllServForSub(int subId);

    void setServForSub(int subId, int servId);

    void delServiceForSub(int subId, int servId);

    void replenishBalance(int subId, double amount);

    boolean checkBalance(int subId, double amount);



}
