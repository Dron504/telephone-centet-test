package com.avakulenko.telephone.center.service.Impl;

import com.avakulenko.telephone.center.dao.AccountDAO;
import com.avakulenko.telephone.center.dao.ServDAO;
import com.avakulenko.telephone.center.dao.SubscriberDAO;
import com.avakulenko.telephone.center.domain.Account;
import com.avakulenko.telephone.center.domain.Serv;
import com.avakulenko.telephone.center.domain.Subscriber;
import com.avakulenko.telephone.center.exception.InsufficientFundsException;
import com.avakulenko.telephone.center.service.AccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

/**
 * Created by Andrey on 20.03.2016.
 */
@Service
public class AccountServiceImpl implements AccountService {

    @Autowired
    AccountDAO accountDAO;
    @Autowired
    ServDAO servDAO;
    @Autowired
    SubscriberDAO subscriberDAO;

    @Override
    @Transactional
    public void addAccount(int subId, int servId) {
        Account account = new Account(subId, servId);
       account.setAmount(servDAO.getById(account.getServicId()).getPrice());
        accountDAO.insert(account);
    }

    @Override
    public  Map<Account, String> getAllAccountsBySub(int subId) {
        Map<Account, String> map = new HashMap<>();
       List<Serv> servList = servDAO.getAll();
        List<Account> accountList = accountDAO.getAllAccountsForSub(subId);
        for (Account account: accountList) {
            for (Serv serv : servList) {
                if (account.getServicId() == serv.getId()){
                    map.put(account, serv.getName());
                }
            }
        }
        return map;
    }

    @Override
    public Map<Account, String> getUnpaidAccountsBySub(int subId) {
        Map<Account, String> map = new HashMap<>();
        List<Serv> servList = servDAO.getAll();
        List<Account> accountList = accountDAO.getAllAccountsForSub(subId);
        for (Account account: accountList) {
            for (Serv serv : servList) {
                if (!account.isPaid() && account.getServicId() == serv.getId()){
                    map.put(account, serv.getName());
                }
            }
        }
        return map;
    }

    @Override
    @Transactional
    public void setPaidAccount(int accountId) throws InsufficientFundsException{
        Account account = accountDAO.getById(accountId);
        Subscriber subscriber = subscriberDAO.getById(account.getSubscriberId());
        if (subscriber.getBalance()- account.getAmount() < 0 ) {
            throw new InsufficientFundsException();
        }
        subscriber.setBalance(subscriber.getBalance()- account.getAmount());
        account.setPaid(true);
        account.setPaidDate(new Date());
        subscriberDAO.update(subscriber);
        accountDAO.update(account);
    }
}
