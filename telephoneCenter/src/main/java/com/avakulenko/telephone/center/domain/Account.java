package com.avakulenko.telephone.center.domain;

import java.util.Date;

public class Account {
    private int id;
    private boolean paid;
    private int servicId;
    private int subscriberId;
    private double amount;
    private Date creationDate;
    private Date paidDate;

    public double getAmount() {
        return amount;
    }

    public Account() {
    }

    public Account(int subscriberId, int servicId) {
        this.subscriberId = subscriberId;
        this.servicId = servicId;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public boolean isPaid() {
        return paid;
    }

    public void setPaid(boolean paid) {
        this.paid = paid;
    }

    public int getServicId() {
        return servicId;
    }

    public void setServicId(int servicId) {
        this.servicId = servicId;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public Date getPaidDate() {
        return paidDate;
    }

    public void setPaidDate(Date paidDate) {
        this.paidDate = paidDate;
    }

    public int getSubscriberId() {
        return subscriberId;
    }

    public void setSubscriberId(int subscriberId) {
        this.subscriberId = subscriberId;
    }
}
