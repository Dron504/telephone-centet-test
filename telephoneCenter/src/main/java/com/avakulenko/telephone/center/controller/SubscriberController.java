package com.avakulenko.telephone.center.controller;

import com.avakulenko.telephone.center.domain.Serv;
import com.avakulenko.telephone.center.domain.Subscriber;
import com.avakulenko.telephone.center.service.AccountService;
import com.avakulenko.telephone.center.service.ServService;
import com.avakulenko.telephone.center.service.SubscriberService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

@Controller
public class SubscriberController {
    private static Logger logger = Logger.getLogger(SubscriberController.class);

    @Autowired
    SubscriberService subscriberService;
    @Autowired
    ServService servService;
    @Autowired
    AccountService accountService;

    @RequestMapping("/getListSub")
    public ModelAndView getSubscriberList() {
        List<Subscriber> subList = subscriberService.getAllSubscriber();
        return new ModelAndView("subList", "subList", subList);
    }

    @RequestMapping("/registerSub")
    public ModelAndView registerSubscriber(@ModelAttribute Subscriber subscriber) {
        return new ModelAndView("registerSub");
    }

    @RequestMapping("/insertSub")
    public String insertData(@ModelAttribute Subscriber subscriber) {
        subscriberService.addSubscriber(subscriber);
        return "redirect:/getListSub";
    }

    @RequestMapping("/setBlockSub")
    public String blockSubscriber(@RequestParam String id) {
        subscriberService.blockSubscriber(Integer.parseInt(id));
        return "redirect:/getListSub";
    }

    @RequestMapping("/setUnBlockSub")
    public String unBlockSubscriber(@RequestParam String id) {
        subscriberService.unBlockSubscriber(Integer.parseInt(id));
        return "redirect:/getListSub";
    }

    @RequestMapping("/getSub")
    public ModelAndView getSubscriberView(@RequestParam String id) {
        Subscriber subscriber = subscriberService.getById(Integer.parseInt(id));
        return new ModelAndView("subscriberPage", "subscriber", subscriber);
    }

    @RequestMapping("/setServForSub")
    public String setServForSub(@RequestParam Integer subId, Integer servId, @ModelAttribute Subscriber subscriber, Model model) {
        subscriberService.setServForSub(subId, servId);
        accountService.addAccount(subId, servId);
        model.addAttribute("id", subId);
        return "redirect:/getSub";
    }

    @RequestMapping("/addNewServForSub")
    public ModelAndView addNewServForSub(@RequestParam String id, @ModelAttribute Subscriber subscriber) {
        List<Serv> servList = servService.getAllServices();
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("subId", id);
        modelAndView.addObject("servList", servList);
        modelAndView.setViewName("/setNewServForSub");
        return modelAndView;
    }

    @RequestMapping("/delServForSub")
    public String delServForSub(@RequestParam String subId, String servId, @ModelAttribute Subscriber subscriber, Model model) {
        subscriberService.delServiceForSub(Integer.parseInt(subId), Integer.parseInt(servId));
        model.addAttribute("id", subId);
        return "redirect:/getSub";
    }

    @RequestMapping("/replenishBalance")
    public String replenishBalance(@RequestParam String subId, String amount, @ModelAttribute Subscriber subscriber, Model model) {
        subscriberService.replenishBalance(Integer.parseInt(subId), (amount != null) ? Double.parseDouble(amount) : 0);
        model.addAttribute("id", subId);
        return "redirect:/getSub";
    }


}
