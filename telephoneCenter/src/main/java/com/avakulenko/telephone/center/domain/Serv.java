package com.avakulenko.telephone.center.domain;

/**
 * Created by Andrey on 11.03.2016.
 */
public class Serv {
    private int id;
    private String name;
    private double price;

    public Serv() {
    }

    public Serv(String name, double price) {
        this.name = name;
        this.price = price;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Serv serv = (Serv) o;

        return id == serv.id;

    }

    @Override
    public int hashCode() {
        return id;
    }

    @Override
    public String toString() {
        return "Serv{" +
                ",name='" + name + '\'' +
                ", price=" + price +
                '}';
    }
}
