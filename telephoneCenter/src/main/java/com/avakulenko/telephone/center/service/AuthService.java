package com.avakulenko.telephone.center.service;

import com.avakulenko.telephone.center.domain.Subscriber;
import com.avakulenko.telephone.center.dto.AuthResponse;

public interface AuthService {
    AuthResponse auth(String login, String password);

    Subscriber getBySessionId(String uuid);

    void delBySessionId(String uuid);

}
