package com.avakulenko.telephone.center.service.Impl;


import com.avakulenko.telephone.center.dao.ServDAO;
import com.avakulenko.telephone.center.dao.SubscriberDAO;
import com.avakulenko.telephone.center.domain.Serv;
import com.avakulenko.telephone.center.domain.Subscriber;
import com.avakulenko.telephone.center.service.SubscriberService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Andrey on 17.03.2016.
 */

@Service
public class SubscriberServiceImpl implements SubscriberService {
    @Autowired
    SubscriberDAO subscriberDAO;
    @Autowired
    ServDAO servDAO;

    @Transactional
    public void addSubscriber(Subscriber subscriber) {
        subscriberDAO.insert(subscriber);

    }

    public Subscriber getById(int subId) {
        return subscriberDAO.getById(subId);
    }

    @Transactional
    public void removeSubscriber(int subId) {
        subscriberDAO.delete(subId);
    }

    @Transactional
    public List<Subscriber> getAllSubscriber() {
        return subscriberDAO.getAll();
    }

    @Transactional
    public void blockSubscriber(int subId) {
        Subscriber subscriber = subscriberDAO.getById(subId);
        if (subscriber != null) {
            subscriber.isBlocked(true);
            subscriberDAO.update(subscriber);
        }
    }

    @Transactional
    public void unBlockSubscriber(int subId) {
        Subscriber subscriber = subscriberDAO.getById(subId);
        if (subscriber != null) {
            subscriber.isBlocked(false);
            subscriberDAO.update(subscriber);
        }
    }

    public List<Serv> getAllServForSub(int subId) {
        return null;
    }

    @Transactional
    public void setServForSub(int subId, int servId) {
        List<Serv> servs = new ArrayList<>();
        Serv service = servDAO.getById(servId);
        if (service != null) {
            servs.add(service);
            servDAO.setForSub(subId, servs);
        }
    }

    @Transactional
    public void delServiceForSub(int subId, int servId) {
        List<Serv> servs = new ArrayList<Serv>();
        Serv service = servDAO.getById(servId);
        if (service != null) {
            servs.add(service);
            servDAO.delServicesSub(subId, servs);
        }
    }

    @Override
    public void replenishBalance(int subId, double amount) {
        Subscriber subscriber = subscriberDAO.getById(subId);
        subscriber.setBalance(subscriber.getBalance() + amount);
        subscriberDAO.update(subscriber);
    }

    @Override
    public boolean checkBalance(int subId, double amount) {
        return  subscriberDAO.getById(subId).getBalance() - amount >= 0;
    }
}
