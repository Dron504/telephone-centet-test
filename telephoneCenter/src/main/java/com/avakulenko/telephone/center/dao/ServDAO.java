package com.avakulenko.telephone.center.dao;

import com.avakulenko.telephone.center.domain.Serv;

import java.util.List;

public interface ServDAO {
    //create
    void insert(Serv serv);

    //get
    Serv getById(int id);

    // update
    void update(Serv serv);

    // delete
    void delete(Serv serv);

    // get all
    List<Serv> getAll();

    //get all servs for subscriber
    List<Serv> getBySub(int idSub);

    // set new serv for subscriber
    void setForSub(int idSub, List<Serv> servs);

    // delete the list of servs at the subscriber
    void delServicesSub(int idSub, List<Serv> servs);
}
