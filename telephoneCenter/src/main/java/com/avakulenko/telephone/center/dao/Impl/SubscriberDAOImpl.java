package com.avakulenko.telephone.center.dao.Impl;

import com.avakulenko.telephone.center.dao.AccountDAO;
import com.avakulenko.telephone.center.dao.ServDAO;
import com.avakulenko.telephone.center.dao.SubscriberDAO;
import com.avakulenko.telephone.center.domain.Serv;
import com.avakulenko.telephone.center.domain.Subscriber;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Impl Dao for subscriber
 */
@Repository
public class SubscriberDAOImpl implements SubscriberDAO {

    private static Logger logger = Logger.getLogger(SubscriberDAOImpl.class);
    @Autowired
    //@Qualifier("dataSource")
    private DataSource dataSources;  //dataSource from connection pool JNDI
    @Autowired
    private ServDAO servDAO;
    @Autowired
    private AccountDAO accountDAO;

    public void insert(Subscriber s) {
        String sql = "INSERT INTO telephone_center.users (first_name, last_name, tel_number, login, password, is_blocked, balance) VALUES (?,?,?,?,?,?,?);";
        try {
            JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSources);
            jdbcTemplate.update(sql, new Object[]{s.getFirstName(), s.getLastName(), s.getNumber(), s.getLogin(),
                    s.getPassword(), s.isBlocked(), s.getBalance()});
            if (s.getServs() != null && s.getServs().size() > 0) {
                servDAO.setForSub(s.getId(), s.getServs());
            }
        } catch (Exception e) {
            logger.error(e);
        }
    }


    public Subscriber getById(int id) {
        String sql = "SELECT id, first_name, last_name, tel_number, login, password, is_blocked, balance, is_admin  " +
                "FROM telephone_center.users WHERE id = ?";
        Subscriber sub = null;
        try {
            JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSources);
            sub = jdbcTemplate.queryForObject(sql, new Object[]{id}, new RowMapper<Subscriber>() {
                public Subscriber mapRow(ResultSet resultSet, int i) throws SQLException {
                    Subscriber sub = new Subscriber();
                    sub.setId(resultSet.getInt("id"));
                    sub.setFirstName(resultSet.getString("first_name"));
                    sub.setLastName(resultSet.getString("last_name"));
                    sub.setLogin(resultSet.getString("login"));
                    sub.setPassword(resultSet.getString("password"));
                    sub.setNumber(resultSet.getString("tel_number"));
                    sub.isBlocked((resultSet.getInt("is_Blocked") == 1));
                    sub.setBalance(resultSet.getDouble("balance"));
                    sub.setAdmin((resultSet.getInt("is_admin") == 1));
                    return sub;
                }
            });
            List<Serv> servs = servDAO.getBySub(id);
            if (servs != null && servs.size() > 0) {
                sub.setServs(servs);
            }
        } catch (Exception e) {
            logger.error(e);
        }
        return sub;
    }

    public void update(Subscriber subscriber) {
        String sql = "UPDATE telephone_center.users SET first_name = ?, last_name = ? , tel_number = ?, password = ?, is_blocked = ? ,balance = ? WHERE id = ? ";
        try {
            JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSources);
            jdbcTemplate.update(sql, new Object[]{subscriber.getFirstName(), subscriber.getLastName(), subscriber.getNumber(),
                    subscriber.getPassword(), subscriber.isBlocked(), subscriber.getBalance(), subscriber.getId()});
        } catch (Exception e) {
            logger.error(e);
        }
    }

    public void delete(int subId) {
        String sql = "DELETE from telephone_center.users WHERE id = ?";
        try {
            JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSources);
            jdbcTemplate.update(sql, subId);
        } catch (Exception e) {
            logger.error(e);
        }
    }

    public List<Subscriber> getAll() {
        List<Subscriber> subscribers = new ArrayList<Subscriber>();
        String sql = "SELECT id, first_name, last_name, tel_number, login, password, is_blocked, balance, is_admin FROM telephone_center.users";
        try {
            JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSources);
            List<Map<String, Object>> subRows = jdbcTemplate.queryForList(sql);
            for (Map<String, Object> subRow : subRows) {
                Subscriber sub = new Subscriber();
                sub.setId(Integer.parseInt(String.valueOf(subRow.get("id"))));
                sub.setFirstName(String.valueOf(subRow.get("first_name")));
                sub.setLastName(String.valueOf(subRow.get("last_name")));
                sub.setLogin(String.valueOf(subRow.get("login")));
                sub.setPassword(String.valueOf(subRow.get("password")));  // del!!
                sub.setNumber(String.valueOf(subRow.get("tel_number")));
                sub.isBlocked((Integer.parseInt(String.valueOf(subRow.get("is_blocked"))) == 1));
                sub.setBalance(Double.parseDouble(String.valueOf(subRow.get("balance"))));
                sub.setAdmin((Integer.parseInt(String.valueOf(subRow.get("is_admin"))) == 1));
                subscribers.add(sub);
            }

        } catch (Exception e) {
            logger.error(e);
        }

        return subscribers;
    }

    public Integer checkUserCredentials(String login, String password) {
        String sql = "SELECT id FROM telephone_center.users WHERE login = ? AND password = ?";
        Integer subId = null;
        try {
            JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSources);
            subId = jdbcTemplate.queryForObject(sql, new Object[]{login, password}, new RowMapper<Integer>() {
                @Override
                public Integer mapRow(ResultSet resultSet, int i) throws SQLException {
                    return resultSet.getInt("id");
                }
            });
        } catch (Exception e) {
            logger.error(e);
        }
        return subId;
    }
}
