package com.avakulenko.telephone.center.dao;

import com.avakulenko.telephone.center.domain.Account;

import java.util.List;

/**
 * interface for access to accountOld
 */
public interface AccountDAO {

    //create
    void insert(Account account);

    //get account
    Account getById(int idAccount);

    // update accountOld title
    void update(Account account);

    // delete account
    void delete(Account account);

    // select all accounts for subscriber
    List<Account> getAllAccountsForSub(int sunId);

    // get all accounts
    List<Account> getAll();


}
