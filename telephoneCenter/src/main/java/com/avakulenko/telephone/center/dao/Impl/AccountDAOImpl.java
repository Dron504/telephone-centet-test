package com.avakulenko.telephone.center.dao.Impl;

import com.avakulenko.telephone.center.dao.AccountDAO;
import com.avakulenko.telephone.center.domain.Account;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

@Repository
public class AccountDAOImpl implements AccountDAO {

    private static Logger logger = Logger.getLogger(AccountDAOImpl.class);
    @Autowired
    private DataSource dataSources;  //dataSource from connection pool JNDI

    @Override
    public void insert(Account account) {
        String sql = "INSERT INTO telephone_center.account_users (user_id, service_id, is_paid, amount)\n" +
                "VALUES(?,?,?,?)";
        try {
            JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSources);
            jdbcTemplate.update(sql, new Object[]{account.getSubscriberId(), account.getServicId(), account.isPaid() ? 1 : 0, account.getAmount()});
        } catch (Exception e) {
            logger.error(e);
        }
    }

    @Override
    public Account getById(int idAccount) {
        String sql = "SELECT id, user_id, service_id, is_paid, paid_dt, amount  FROM telephone_center.account_users WHERE  id = ?";
        Account account = null;
        try {
            JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSources);
            account = jdbcTemplate.queryForObject(sql, new Object[]{idAccount}, new RowMapper<Account>() {
                @Override
                public Account mapRow(ResultSet resultSet, int i) throws SQLException {
                    Account account = new Account();
                    account.setId(resultSet.getInt("id"));
                    account.setSubscriberId(resultSet.getInt("user_id"));
                    account.setServicId(resultSet.getInt("service_id"));
                    account.setPaid((resultSet.getInt("is_paid") == 1));
                    Date paidDt = resultSet.getDate("paid_dt");
                    if (paidDt != null) account.setPaidDate(paidDt);
                    account.setAmount(resultSet.getDouble("amount"));
                    return account;
                }
            });
        } catch (Exception e) {
            logger.error(e);
        }
        return account;
    }

    @Override
    public void update(Account account) {
        String sql = "UPDATE telephone_center.account_users SET user_id = ? ,service_id = ?, is_paid = ?, paid_dt = ? WHERE id = ?";
        String pattern = "yyyy-MM-dd";
        SimpleDateFormat formatter = new SimpleDateFormat(pattern);
        String mysqlDateString = "";
        if (account.getPaidDate() != null) {
            mysqlDateString = formatter.format(account.getPaidDate());
        }
        try {
            JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSources);
            jdbcTemplate.update(sql, new Object[]{account.getSubscriberId(),
                    account.getServicId(), account.isPaid(), mysqlDateString, account.getId()});
        } catch (Exception e) {
            logger.error(e);
        }

    }

    @Override
    public void delete(Account account) {
        String sql = "DELETE FROM telephone_center.account_users WHERE id = ?";
        try {
            JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSources);
            jdbcTemplate.update(sql, account.getId());
        } catch (Exception e) {
            logger.error(e);
        }
    }

    @Override
    public List<Account> getAllAccountsForSub(int subId) {
        String sql = "SELECT id, user_id, service_id, is_paid, paid_dt, amount  FROM telephone_center.account_users WHERE  user_id = ?";
        List<Account> accounts = new ArrayList<>();
        String pattern = "yyyy-MM-dd";
        SimpleDateFormat formatter = new SimpleDateFormat(pattern);
        // Date paidDate;
        try {
            JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSources);
            List<Map<String, Object>> accsRows = jdbcTemplate.queryForList(sql, subId);
            for (Map<String, Object> accRow : accsRows) {
                Account account = new Account();
                account.setId(Integer.parseInt(String.valueOf(accRow.get("id"))));
                account.setSubscriberId(Integer.parseInt(String.valueOf(accRow.get("user_id"))));
                account.setServicId(Integer.parseInt(String.valueOf(accRow.get("service_id"))));
                account.setPaid((Integer.parseInt(String.valueOf(accRow.get("is_paid")))) == 1);
                if ((accRow.get("paid_dt") != null)) {
                    account.setPaidDate(formatter.parse((String.valueOf(accRow.get("paid_dt")))));
                }
                account.setAmount(Double.parseDouble(String.valueOf(accRow.get("amount"))));
                accounts.add(account);
            }
        } catch (Exception e) {
            logger.error(e);
            e.printStackTrace();
        }
        return accounts;
    }

    @Override
    public List<Account> getAll() {
        String sql = "SELECT id, user_id, service_id, is_paid, paid_dt  FROM telephone_center.account_users WHERE";
        List<Account> accounts = new ArrayList<>();
        try {
            JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSources);
            List<Map<String, Object>> accsRows = jdbcTemplate.queryForList(sql);
            for (Map<String, Object> accRow : accsRows) {
                Account account = new Account();
                account.setId(Integer.parseInt(String.valueOf(accRow.get("id"))));
                account.setSubscriberId(Integer.parseInt(String.valueOf(accRow.get("user_id"))));
                account.setServicId(Integer.parseInt(String.valueOf(accRow.get("service_id"))));
                account.setPaid((Integer.parseInt(String.valueOf(accRow.get("is_paid")))) == 1);
                account.setPaidDate(new Date(String.valueOf(accRow.get("paid_dt"))));
                account.setAmount(Double.parseDouble(String.valueOf("amount")));
                accounts.add(account);
            }
        } catch (Exception e) {
            logger.error(e);
        }
        return accounts;
    }
}
