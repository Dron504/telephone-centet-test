package com.avakulenko.telephone.center.service.Impl;

import com.avakulenko.telephone.center.dao.SubscriberDAO;
import com.avakulenko.telephone.center.domain.Subscriber;
import com.avakulenko.telephone.center.dto.AuthResponse;
import com.avakulenko.telephone.center.service.AuthService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Map;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

@Service
public class AuthServiceImpl implements AuthService {
    private static Logger logger = Logger.getLogger(AuthServiceImpl.class);
    private Map<String, Subscriber> subscriberHolder = new ConcurrentHashMap<>();
    @Autowired
    private SubscriberDAO subscriberDAO;

    public AuthResponse auth(String login, String password) {
        AuthResponse authResponse = new AuthResponse();
        try {
            Integer subId = subscriberDAO.checkUserCredentials(login, password);
            if (subId == null) return null;

            Subscriber subscriber = subscriberDAO.getById(subId);
            String uuid = UUID.randomUUID().toString();
            subscriberHolder.put(uuid, subscriber);
            authResponse.setSubscriber(subscriber);
            authResponse.setUuid(uuid);
        } catch (Exception e) {
            logger.error(e);
        }
        return authResponse;
    }

    @Override
    public Subscriber getBySessionId(String uuid) {
        return subscriberHolder.get(uuid);
    }

    @Override
    public void delBySessionId(String uuid) {
        subscriberHolder.remove(uuid);
    }
}
