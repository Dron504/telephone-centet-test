package com.avakulenko.telephone.center.service;

import com.avakulenko.telephone.center.domain.Account;
import com.avakulenko.telephone.center.exception.InsufficientFundsException;

import java.util.List;
import java.util.Map;

/**
 * Created by Andrey on 19.03.2016.
 */
public interface AccountService {

    void addAccount(int subId,  int servId);

    Map<Account, String> getAllAccountsBySub(int subId);

    Map<Account, String> getUnpaidAccountsBySub(int subId);

    void setPaidAccount(int accountId) throws InsufficientFundsException;

}
