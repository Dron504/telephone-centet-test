package com.avakulenko.telephone.center.dto;

import com.avakulenko.telephone.center.domain.Subscriber;

public class AuthResponse {
    private String uuid;
    private Subscriber subscriber;

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public Subscriber getSubscriber() {
        return subscriber;
    }

    public void setSubscriber(Subscriber subscriber) {
        this.subscriber = subscriber;
    }
}
