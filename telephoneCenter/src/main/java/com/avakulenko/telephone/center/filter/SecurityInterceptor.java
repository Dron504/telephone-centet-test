package com.avakulenko.telephone.center.filter;

import com.avakulenko.telephone.center.service.AuthService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.HashSet;
import java.util.Set;

@Component
public class SecurityInterceptor extends HandlerInterceptorAdapter {


    private static final Set<String> ADMIN_RECURSE = new HashSet<>();
    private static final Set<String> SUBSCRIBER_RECURSE = new HashSet<>();
    private static Logger logger = Logger.getLogger(SecurityInterceptor.class);

    static {
        ADMIN_RECURSE.add("/getListSub");
        ADMIN_RECURSE.add("/registerSub");
        ADMIN_RECURSE.add("/insertSub");
        ADMIN_RECURSE.add("/setBlockSub");
        ADMIN_RECURSE.add("/setUnBlockSub");
        ADMIN_RECURSE.add("/registerServ");
        ADMIN_RECURSE.add("/insertServ");
        ADMIN_RECURSE.add("/getUnpaidAccountsBySub");
        SUBSCRIBER_RECURSE.add("/getSub");
        SUBSCRIBER_RECURSE.add("/setServForSub");
        SUBSCRIBER_RECURSE.add("/addNewServForSub");
        SUBSCRIBER_RECURSE.add("/delServForSub");
        SUBSCRIBER_RECURSE.add("/getListServ");
        SUBSCRIBER_RECURSE.add("/payAccount");
        SUBSCRIBER_RECURSE.add("/getAccountsForSub");
        SUBSCRIBER_RECURSE.add("/replenishBalance");
    }

    @Autowired
    private AuthService authService;

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        String uri = request.getRequestURI();
        logger.info("User try to request:" + uri);

        if (request.getRequestURI().equals("/login") || request.getRequestURI().equals("/logout"))
            return true;

        Cookie[] cookies = request.getCookies();
        String sessionId = null;
        if (cookies != null) {
            for (int i = 0; i < cookies.length; i++) {
                if (cookies[i].getName().equals("sessionId")) {
                    sessionId = cookies[i].getValue();
                    break;
                }
            }
            if (sessionId == null) {
                response.sendError(403);
                return false;
            }

            if (authService.getBySessionId(sessionId).isAdmin()) {
                for (String adminUrl: ADMIN_RECURSE) {
                    if (uri.startsWith(adminUrl)) return true;
                }
            } else  {
                for (String adminUrl: SUBSCRIBER_RECURSE) {
                    if (uri.startsWith(adminUrl)) return true;
                }
            }
            response.sendError(403);
            return false;
        }
        return false;

    }
}

