package com.avakulenko.telephone.center.dao.Impl;

import com.avakulenko.telephone.center.dao.ServDAO;
import com.avakulenko.telephone.center.domain.Serv;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Repository
public class ServDAOImpl implements ServDAO {
    private static Logger logger = Logger.getLogger(ServDAOImpl.class);
    @Autowired
    private DataSource dataSources;  //dataSource from connection pool JNDI

    public void insert(Serv serv) {
        String sql = "INSERT into telephone_center.dim_services(name, price) VALUES (?, ?)";
        try {
            JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSources);
            jdbcTemplate.update(sql, new Object[]{serv.getName(), serv.getPrice()});
        } catch (Exception e) {
            logger.error(e);
        }

    }

    public Serv getById(int id) {
        String sql = "SELECT id, name, price FROM telephone_center.dim_services WHERE id = ?";
        Serv serv = null;
        try {
            JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSources);
            serv = jdbcTemplate.queryForObject(sql, new Object[]{id}, new RowMapper<Serv>() {
                public Serv mapRow(ResultSet resultSet, int i) throws SQLException {
                    Serv ser = new Serv();
                    ser.setId(resultSet.getInt("id"));
                    ser.setName(resultSet.getString("name"));
                    ser.setPrice(resultSet.getDouble("price"));
                    return ser;
                }
            });
        } catch (Exception e) {
            logger.error(e);
        }
        return serv;
    }

    public void update(Serv serv) {
        String sql = "UPDATE telephone_center.dim_services SET name = ?, price = ? WHERE id = ?";
        try {
            JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSources);
            jdbcTemplate.update(sql, new Object[]{serv.getName(), serv.getPrice(), serv.getId()});

        } catch (Exception e) {
           logger.error(e);
        }

    }

    public void delete(Serv serv) {
        String sql = "DELETE FROM telephone_center.dim_services WHERE id = ?";
        try {
            JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSources);
            jdbcTemplate.update(sql, serv.getId());
        } catch (Exception e) {
            logger.error(e);
        }


    }

    public List<Serv> getAll() {
        String sql = "SELECT id, name, price FROM telephone_center.dim_services";
        List<Serv> servs = new ArrayList<>();
        try {
            JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSources);
            List<Map<String, Object>> serRows = jdbcTemplate.queryForList(sql);
            for (Map<String, Object> servRow : serRows) {
                Serv serv = new Serv();
                serv.setId(Integer.parseInt(String.valueOf(servRow.get("id"))));
                serv.setName(String.valueOf(servRow.get("name")));
                serv.setPrice(Double.parseDouble(String.valueOf(servRow.get("price"))));
                servs.add(serv);
            }
        } catch (Exception e) {
            logger.error(e);
        }
        return servs;
    }

    public List<Serv> getBySub(int idSub) {
        String sql = "SELECT d.id, d.name, d.price FROM telephone_center.user_services_ref u " +
                " JOIN  telephone_center.dim_services d ON u.service_id = d.id " +
                "WHERE u.dt_from <= now() AND u.dt_to >= now() AND u.user_id = ?";
        List<Serv> servs = new ArrayList<>();
        try {
            JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSources);
            List<Map<String, Object>> serRows = jdbcTemplate.queryForList(sql, idSub);
            for (Map<String, Object> servRow : serRows) {
                Serv serv = new Serv();
                serv.setId(Integer.parseInt(String.valueOf(servRow.get("id"))));
                serv.setName(String.valueOf(servRow.get("name")));
                serv.setPrice(Double.parseDouble(String.valueOf(servRow.get("price"))));
                servs.add(serv);
                logger.info(sql + " " + idSub);
            }
        } catch (Exception e) {
            logger.error(e);
        }
        return servs;
    }

    public void setForSub(int idSub, List<Serv> servs) {
        String sql = "INSERT INTO telephone_center.user_services_ref  (user_id, service_id) VALUES (?, ?)";
        try {
            JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSources);
            for (Serv serv : servs) {
                logger.info(sql + idSub + " " + serv.getId());  ///!!!
                jdbcTemplate.update(sql, idSub, serv.getId());
            }
        } catch (Exception e) {
            logger.error(e);
        }

    }

    public void delServicesSub(int idSub, List<Serv> servs) {
        String sql = "DELETE FROM telephone_center.user_services_ref WHERE user_id = ? AND service_id = ?";
        try {
            JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSources);
            for (Serv serv : servs) {
                jdbcTemplate.update(sql, idSub, serv.getId());
            }
        } catch (Exception e) {
            logger.error(e);
        }
    }
}
