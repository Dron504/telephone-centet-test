package com.avakulenko.telephone.center.service.Impl;

import com.avakulenko.telephone.center.dao.ServDAO;
import com.avakulenko.telephone.center.domain.Serv;
import com.avakulenko.telephone.center.service.ServService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by Andrey on 19.03.2016.
 */
@Service
public class ServServiceImpl implements ServService {
    @Autowired
    ServDAO servDAO;

    public void addService(Serv service) {
        servDAO.insert(service);
    }


    public void deleteService(Serv service) {
        servDAO.delete(service);
    }

    public List<Serv> getAllServices() {
        return servDAO.getAll();
    }

    public Serv getServiceById(int servId) {
        return servDAO.getById(servId);
    }
}
