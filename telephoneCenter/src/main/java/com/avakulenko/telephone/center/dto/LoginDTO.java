package com.avakulenko.telephone.center.dto;

/**
 * Created by Andrey on 19.03.2016.
 */
public class LoginDTO {
    private String userName;
    private String password;

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }


}
