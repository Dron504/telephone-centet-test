package com.avakulenko.telephone.center.controller;

import com.avakulenko.telephone.center.domain.Subscriber;
import com.avakulenko.telephone.center.dto.AuthResponse;
import com.avakulenko.telephone.center.dto.LoginDTO;
import com.avakulenko.telephone.center.service.AuthService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Controller
public class AuthController {
    private static Logger logger = Logger.getLogger(AuthController.class);

    @Autowired
    private AuthService authService;

    @RequestMapping(method = RequestMethod.GET, value = "/login")
    public String getLoginPage(Model model) {
        model.addAttribute("errorMsg", "");
        model.addAttribute("loginDTO", new LoginDTO());
        return "loginPage";
    }

    @RequestMapping(method = RequestMethod.POST, value = "/login")
    public String doLogin(@ModelAttribute LoginDTO loginDTO, HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Model model) {
        AuthResponse authResponse = authService.auth(loginDTO.getUserName(), loginDTO.getPassword());
        if (authResponse == null) {
            model.addAttribute("errorMsg", "Login/Password is invalid");
            return "loginPage";
        }

        String sessionId = authResponse.getUuid();
        Subscriber subscriber = authResponse.getSubscriber();
        Cookie[] cookies = httpServletRequest.getCookies();
        String sessionId1 = null;
        for (int i = 0; i < cookies.length; i++) {
            if (cookies[i].getName().equals("sessionId")) {
                sessionId1 = cookies[i].getValue();
                break;
            }
        }
        if (sessionId1 == null) {
            httpServletResponse.addCookie(new Cookie("sessionId", sessionId));
        }
        Subscriber subscriber1 = authService.getBySessionId(sessionId);
        if (subscriber1 == null) {
            return "loginPage";
        }
        if (subscriber.isAdmin()) {
            return "redirect:/getListSub";
        } else {
            model.addAttribute("subscriber", subscriber);
            return "subscriberPage";
        }
    }

    @RequestMapping("/logout")
    public String logout(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse) {
        try {
            httpServletResponse.addCookie(new Cookie("sessionId", "0"));
            Cookie[] cookies = httpServletRequest.getCookies();
            String sessionId1 = null;
            for (int i = 0; i < cookies.length; i++) {
                if (cookies[i].getName().equals("sessionId")) {
                    sessionId1 = cookies[i].getValue();
                    break;
                }
            }
            if (sessionId1 != null) authService.delBySessionId(sessionId1);
            httpServletRequest.logout();
        } catch (ServletException e) {
            logger.error(e);
        }
        return "redirect:/login";
    }

}
