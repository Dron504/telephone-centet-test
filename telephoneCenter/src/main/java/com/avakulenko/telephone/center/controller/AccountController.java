package com.avakulenko.telephone.center.controller;

import com.avakulenko.telephone.center.domain.Account;
import com.avakulenko.telephone.center.exception.InsufficientFundsException;
import com.avakulenko.telephone.center.service.AccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import java.util.Map;


@Controller
public class AccountController {
    @Autowired
    AccountService accountService;


    @RequestMapping("/getAccountsForSub")
    public ModelAndView getAccountsForSub(@RequestParam Integer subId) {
        Map<Account, String> mapAccount = accountService.getAllAccountsBySub(subId);
        return new ModelAndView("/accountForSub", "mapAccount", mapAccount);
    }

    @RequestMapping("/payAccount")
    public String payAccount(@RequestParam Integer accountId, Integer subId, Model model) {
        model.addAttribute("subId", subId);
        try {
            accountService.setPaidAccount(accountId);
        } catch (InsufficientFundsException e) {
            model.addAttribute("errorMsg", "Insufficient funds");
        } finally {
            return "redirect:/getAccountsForSub";
        }
    }

    @RequestMapping("/getUnpaidAccountsBySub")
    public ModelAndView getUnpaidAccountsBySub(@RequestParam Integer id) {
        Map<Account, String> mapAccount = accountService.getUnpaidAccountsBySub(id);
        return new ModelAndView("/accountForSub", "mapAccount", mapAccount);
    }
}
