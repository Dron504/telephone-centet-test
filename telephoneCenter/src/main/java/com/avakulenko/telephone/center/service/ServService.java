package com.avakulenko.telephone.center.service;

import com.avakulenko.telephone.center.domain.Serv;

import java.util.List;

/**
 * Created by Andrey on 19.03.2016.
 */
public interface ServService {

    void addService(Serv serv);

    void deleteService(Serv serv);

    List<Serv> getAllServices();

    Serv getServiceById(int servId);

}
