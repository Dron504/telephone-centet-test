package com.avakulenko.telephone.center.controller;

import com.avakulenko.telephone.center.domain.Serv;
import com.avakulenko.telephone.center.service.ServService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;


@Controller
public class ServController {
    @Autowired
    ServService servService;

    @RequestMapping("/registerServ")
    public ModelAndView registerServ(@ModelAttribute Serv serv) {
        return new ModelAndView("registerServ");
    }

    @RequestMapping("/getListServ")
    public ModelAndView getServList() {
        List<Serv> servList = servService.getAllServices();
        return new ModelAndView("servList", "servList", servList);
    }

    @RequestMapping("/insertServ")
    public String insertData(@ModelAttribute Serv serv) {
        if (serv != null)
            servService.addService(serv);
        return "redirect:/getListServ";
    }


}
