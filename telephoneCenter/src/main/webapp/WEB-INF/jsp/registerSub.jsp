<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
  <title>Telephone Center  </title>
  <style>
    body {
      font-size: 20px;
      color: teal;
      font-family: Calibri;
    }

    td {
      font-size: 15px;
      color: black;
      width: 100px;
      height: 22px;
      text-align: left;
    }

    .heading {
      font-size: 18px;
      color: white;
      background-color: orange;
      border: thick;
    }
  </style>
</head>
<body>
<form:form method="post" action="/logout">
  <table>
    <tr>
      <td><input type="submit" value="logout" /></td>
    </tr>
  </table>
</form:form>
<center>

  <b> Subscriber | Registration Form </b>



  <div>
    <form:form method="post" action="/insertSub" modelAttribute="subscriber">
      <table>
        <tr>
          <td>First Name :</td>
          <td><form:input path="firstName" /></td>
        </tr>
        <tr>
          <td>Last Name :</td>
          <td><form:input path="lastName" /></td>
        </tr>
        <tr>
          <td>Login :</td>
          <td><form:input path="login" /></td>
        </tr>
        <tr>
          <td>Password :</td>
          <td><form:input path="password" /></td>
        </tr>
        <tr>
          <td>Telephone number:</td>
          <td><form:input path="number" /></td>
          <td> </td>
          <td><input type="submit" value="Save" /></td>
        </tr>
        <tr>

          <td colspan="2"><a href="/getListSub">Click Here to See Subscribers List</a></td>
        </tr>
      </table>
    </form:form>
  </div>
</center>
</body>
</html>
