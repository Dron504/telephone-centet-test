<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title></title>
</head>
<body>
<div>

  <span><b>${errorMsg}</b></span>
  <form:form method="post" action="/login" modelAttribute="loginDTO">
    <table>
      <tr>
        <td>Login :</td>
        <td><form:input path="userName" /></td>
      </tr>
      <tr>
        <td>Password :</td>
        <td><form:input path="password" /></td>
      </tr>
      <tr>
        <td><input type="submit" value="login" /></td>
      </tr>
    </table>
  </form:form>
</div>
</body>
</html>
