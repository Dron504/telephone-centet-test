<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
  <title> Telephone Center </title>
  <style>
    body {
      font-size: 20px;
      color: teal;
      font-family: Calibri;
    }

    td {
      font-size: 15px;
      color: black;
      width: 100px;
      height: 22px;
      text-align: center;
    }
    .heading {
      font-size: 18px;
      color: white;
      background-color: orange;
      border: thick;
    }
  </style>
</head>
<body>
<form:form method="post" action="/logout">
    <table>
        <tr>
            <td><input type="submit" value="logout" /></td>
        </tr>
    </table>
</form:form>
  <b>Subscriber  List </b>
  <table border="1">
    <tr>
      <td class="heading">User Id</td>
      <td class="heading">First Name</td>
      <td class="heading">Last Name</td>
      <td class="heading">Login</td>
      <td class="heading">Telephone number</td>
      <td class="heading">Is blocked</td>
      <td class="heading">Balance</td>
      <td class="heading"> </td>
      <td class="heading"> </td>
      <td class="heading"> </td>
    </tr>
    <c:forEach var="subscriber" items="${subList}">
      <tr>
        <td>${subscriber.id}</td>
        <td>${subscriber.firstName}</td>
        <td>${subscriber.lastName}</td>
        <td>${subscriber.login}</td>
        <td>${subscriber.number}</td>
        <td>${ subscriber.blocked ? "Yes" : "No"}</td>
        <td>${subscriber.balance}</td>
        <td><a href="/setBlockSub?id=${subscriber.id}">Block the subscriber</a></td>
        <td><a href="/setUnBlockSub?id=${subscriber.id}">Unblock the subscriber</a></td>
        <td><a href="/getUnpaidAccountsBySub?id=${subscriber.id}">Unpaid accounts</a></td>
      </tr>
    </c:forEach>
    <tr><td colspan="7"><a href="/registerSub">Add New Subscriber</a></td></tr>
  </table>
</body>
</html>
