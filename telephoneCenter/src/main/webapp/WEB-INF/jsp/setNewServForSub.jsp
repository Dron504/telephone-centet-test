<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
  <title>Being Java Guys | Spring DI Hello World</title>
  <style>
    body {
      font-size: 20px;
      color: teal;
      font-family: Calibri;
    }

    td {
      font-size: 15px;
      color: black;
      width: 300px;
      height: 22px;
      text-align: center;
    }
    .heading {
      font-size: 18px;
      color: white;
      /*font: bold;*/
      background-color: orange;
      border: thick;
    }
  </style>
</head>
<body>

<b>Services List  </b>
<table border="1">
  <tr>
    <td class="heading">Service Id</td>
    <td class="heading">Name</td>
    <td class="heading">Price</td>
    <td class="heading">Add service</td>
  </tr>
  <c:forEach var="serv" items="${servList}">
    <tr>
      <td>${serv.id}</td>
      <td>${serv.name}</td>
      <td>${serv.price}</td>
      <td><a href="/setServForSub?subId=${subId}&amp;servId=${serv.id}">Add service</a></td>
    </tr>
  </c:forEach>
</table>
</body>
</html>
