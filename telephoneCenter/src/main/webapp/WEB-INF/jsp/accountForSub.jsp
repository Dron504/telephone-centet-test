<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
  <title>Accounts</title>
  <style>
    body {
      font-size: 20px;
      color: teal;
      font-family: Calibri;
    }

    td {
      font-size: 15px;
      color: black;
      width: 100px;
      height: 22px;
      text-align: center;
    }
    .heading {
      font-size: 18px;
      color: white;
      /*font: bold;*/
      background-color: orange;
      border: thick;
    }
  </style>
</head>
<body>
<form:form method="post" action="/logout">
  <table>
    <tr>
      <td><input type="submit" value="logout" /></td>
    </tr>
  </table>
</form:form>

<b>Accounts List  </b>
<span><b>${errorMsg}</b></span>
<table border="1">
  <tr>
    <td class="heading">Account ID</td>
    <td class="heading">Name</td>
    <td class="heading">Amount</td>
    <td class="heading">Is paid</td>
    <td class="heading">Date of payment</td>
    <td class="heading"> </td>
  </tr>
  <c:forEach var="account" items="${mapAccount}">
    <tr>
      <td>${account.key.id}</td>
      <td>${account.value}</td>
      <td>${account.key.amount}</td>
      <td>${account.key.paid}</td>
      <td>${account.key.paidDate}</td>
      <td> <form action="/payAccount" method="GET" >
         <input type="hidden" name= "accountId" value="${account.key.id}" >
        <input type="hidden" name="subId" value="${account.key.subscriberId}">
        <input type="submit" value="Pay the account" />
      </form>
      </td>
    </tr>
  </c:forEach>
</table>
</body>
</html>