CREATE DATABASE `telephone_center` /*!40100 DEFAULT CHARACTER SET utf8 */;
CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(25) DEFAULT NULL,
  `is_admin` int(11) DEFAULT '0',
  `login` varchar(45) NOT NULL,
  `password` varchar(45) NOT NULL,
  `is_blocked` int(11) DEFAULT NULL,
  `balance` double DEFAULT NULL,
  `last_name` varchar(25) DEFAULT NULL,
  `tel_number` varchar(25) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `login_UNIQUE` (`login`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
CREATE TABLE `user_services_ref` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `service_id` int(11) NOT NULL,
  `dt_from` date DEFAULT '1970-01-01',
  `dt_to` date DEFAULT '2999-01-01',
  PRIMARY KEY (`id`),
  KEY `user_id_idx` (`user_id`),
  KEY `service_id_idx` (`service_id`),
  CONSTRAINT `service_id` FOREIGN KEY (`service_id`) REFERENCES `dim_services` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `user_id` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=40 DEFAULT CHARSET=utf8;
CREATE TABLE `dim_services` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `price` double DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
CREATE TABLE `account_users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `service_id` int(255) NOT NULL,
  `is_paid` int(11) DEFAULT '0',
  `creation_dt` datetime DEFAULT CURRENT_TIMESTAMP,
  `paid_dt` date DEFAULT NULL,
  `amount` double DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `user_idx` (`user_id`),
  KEY `service_d` (`service_id`),
  CONSTRAINT `service_d` FOREIGN KEY (`service_id`) REFERENCES `dim_services` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `user_d` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;
INSERT INTO telephone_center.users (first_name, is_admin, login, password, is_blocked, last_name)  VALUES (
    "admin", 1, "admin", "admin", 0, "admin");