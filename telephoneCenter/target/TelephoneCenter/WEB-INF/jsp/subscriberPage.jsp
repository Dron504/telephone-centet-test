<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
  <title>Telephone Center  </title>
  <style>
    body {
      font-size: 20px;
      color: teal;
      font-family: Calibri;
    }

    td {
      font-size: 15px;
      color: black;
      width: 500px;
      height: 22px;
      text-align: left;
    }

    .heading {
      font-size: 18px;
      color: white;
      background-color: orange;
      border: thick;
    }
  </style>
</head>
<body>
<form:form method="post" action="/logout">
    <table>
        <tr>
            <td><input type="submit" value="logout" /></td>
        </tr>
    </table>
</form:form>

  <b> Subscriber  </b>
  <div>
      <table>
        <tr>
          <td>ID : ${subscriber.id}</td>
        </tr>
          <tr>
          <td>First Name :  ${subscriber.firstName}</td>
        </tr>
        <tr>
          <td>Last Name : ${subscriber.lastName} </td>
        </tr>
        <tr>
            <td>Login :  ${subscriber.login}</td>
        </tr>
          <tr>
              <td>Telephone number :  ${subscriber.number}</td>
          </tr>
          <tr>
              <td>Balance :  ${subscriber.balance}</td>
          </tr>
          <tr>
              <td>Blocked :  ${subscriber.blocked ? "Yes" : "No"}</td>
          </tr>
          <tr>
              <td colspan="2"><a href="/getAccountsForSub?subId=${subscriber.id}">Accounts</a></td>
          </tr>
      </table>

      <form action="/replenishBalance" method="GET" >
          Replenish the balance :  <input type="number" name="amount" size="20" >
          <input type="hidden" name="subId" value="${subscriber.id}">
          <input type="submit" value="Replenish" />
      </form>

      <b>Services List  </b>

      <table border="1">
          <tr>
              <td class="heading">Service Id</td>
              <td class="heading">Name</td>
              <td class="heading">Price</td>
          </tr>
          <c:forEach var="serv" items="${subscriber.servs}">
              <tr>
                  <td>${serv.id}</td>
                  <td>${serv.name}</td>
                  <td>${serv.price}</td>
              </tr>
          </c:forEach>
          <tr><td colspan="7"><a href="/addNewServForSub?id=${subscriber.id}">Add New Service</a></td></tr>
      </table>
  </div>
</body>
</html>