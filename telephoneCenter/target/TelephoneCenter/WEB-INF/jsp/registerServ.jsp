<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
  <title>Telephone Center </title>
  <style>
    body {
      font-size: 20px;
      color: teal;
      font-family: Calibri;
    }

    td {
      font-size: 15px;
      color: black;
      width: 100px;
      height: 22px;
      text-align: left;
    }

    .heading {
      font-size: 18px;
      color: white;
      background-color: orange;
      border: thick;
    }
  </style>
</head>
<body>
<form:form method="post" action="/logout">
  <table>
    <tr>
      <td><input type="submit" value="logout" /></td>
    </tr>
  </table>
</form:form>
<center>


  <b> Service | Registration Form </b>


  <div>
    <form:form method="post" action="/insertServ" modelAttribute="serv">
      <table>
        <tr>
          <td>Name service :</td>
          <td><form:input path="name" /></td>
        </tr>
        <tr>
          <td>Price :</td>
          <td><form:input path="price" /></td>
        </tr>
        <tr>
          <td> </td>
          <td><input type="submit" value="Save" /></td>
        </tr>
        <tr>
          <td colspan="2"><a href="/getListServ">Click Here to See Service List</a></td>
        </tr>
      </table>
    </form:form>
  </div>
</center>
</body>
</html>
